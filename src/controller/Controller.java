package controller;

import java.awt.Cursor;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import model.*;
import vue.*;

public class Controller implements MouseListener,ActionListener {

    Plateau p;
    Board b;
    Vue v;
    public Controller(Vue v,Board b)
    {
        this.v=v;
        this.b=b;
        v.addMouseListener(this); 
    }
    @Override
    public void mouseClicked(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
        
        p.setCursor(handCursor);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Cursor dCursor = new Cursor(Cursor.DEFAULT_CURSOR);
        p.setCursor(dCursor);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (e.getActionCommand().equals("Resoudre")) {
            this.b.nextPlayer();
            this.b.currentPlayer.setFirst();
            Algorithm p =new Paranoid();
            do{
            
                //Timer t=new Timer();
                this.b.isDead();
                String direction = p.operation(b);
                this.b.play(b.currentPlayer.getCouleur(),direction);
                this.b.afficherGrid();
                //this.v.updatep(this.b);
            }while(!this.b.terminal());
        }
    }
}
