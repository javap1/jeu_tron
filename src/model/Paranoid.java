package model;
import java.util.ArrayList;
/**
 * Algorithme Paranoid
 * @see Algorithm
 */
public class Paranoid implements Algorithm {
	@Override
	public String operation(Board game) {
		String directionBestChoice = null;
		int bestChoice = -1001;
                int badChoice = 1001;
		
		
        int x ,y;
	    ArrayList<Integer> coordonee=new ArrayList<>();
		String[] directions = {"N", "E", "S", "W"};
	 	for (String direction : directions) {     
                // System.out.println("is cell movable to " + game.isCellMovableTo(game.currentPlayer.getX(),game.currentPlayer.getY(), direction) + " x : "+game.currentPlayer.getX() + " y  : "+game.currentPlayer.getY() + " direction : " + direction);
                   	
                         if(game.isCellMovableTo(game.currentPlayer.getX(),game.currentPlayer.getY(), direction)){
                                    Board clone = game.CopyBoard();
                                    clone.play(clone.currentPlayer.getCouleur() ,direction);
                                    int paranoid = paranoid(clone, game.currentPlayer.isFirst() ? game.getProfondeur()-1 : game.getProfondeurCoalition()-1, game.currentPlayer);
                                  //  System.out.println("direction "+ direction + "  paranoid : "+ paranoid + " player"+game.currentPlayer.getCouleur() );
                                    if( paranoid > bestChoice){
                      //  System.out.println("paranoid>bestchoice");
                                            bestChoice = paranoid;
                                            directionBestChoice = direction;
                                    }
                            }
                        
                  
		}
		return directionBestChoice;
	}
	
    
	
	/**
	 * Paranoid est un algorithme qui permet de representer un jeu a N joueurs
	 * comme un jeu a 2 joueurs, les N joueurs sont divisés en 2 groupes: 
	 * un joueur et une coalition des autres joueurs.
	 * de ce fait l'implementation de paranoid est relativement similaire a Minimax
	 * @param game une simulation du jeu
	 * @param profondeur profondeur de recherche de l'algorithme
	 * @param player Le joueur qui doit jouer
	 * @return une valeur entiere representant la valeur de ce noeud
	 */
        /* le joueur passé en parametre essaye de minimiser son score lors du tour du joueur adverse pour anticiper ce qu'il va faire , parceque l'autre 
        va toujours essayer de minimiser son score pour le battre*/
	private int paranoid(Board game, int profondeur, Player player){
		int m = 0;
		if(profondeur == 0){
                   // System.out.println("  m0 : "+ game.value(player));
			return game.value(player);
		}
		
		//si c'est un noeud joueur
                if(player.isFirst()){
                    if(game.currentPlayer.isFirst()){
                            m = - 1000;
                            for (Board gameNext : game.listsuccesseurs()) {
                                    int tmp = paranoid(gameNext, profondeur - 1, player);
                                    m = Math.max(m, tmp);
                            }
                    }
                    else{//si c'est un noeud opposant
                            m = 1000;
                            for (Board gameNext : game.listsuccesseurs()) {
                                    int tmp = paranoid(gameNext, profondeur - 1, player);
                                    m = Math.min(m, tmp);    
                            }
                    }
                }
                else {
                    if(game.currentPlayer.isFirst()){
                            m = 1000;
                            for (Board gameNext : game.listsuccesseurs()) {
                                    int tmp = paranoid(gameNext, profondeur - 1, player);
                                    m = Math.min(m, tmp);
                            }
                    }
                    else{//si c'est un noeud opposant
                            m = - 1000;
                            for (Board gameNext : game.listsuccesseurs()) {
                                    int tmp = paranoid(gameNext, profondeur - 1, player);
                                    m = Math.max(m, tmp);
                            }
                    }
                }
                
               // System.out.println("  m : "+ m + " profondeur" + profondeur);
		return m;
	}
}
