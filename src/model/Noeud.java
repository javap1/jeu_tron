package model;


public class Noeud
{
	/**
	 * Coordonnée x du noeud.
	*/	
	protected int x;
	
	/**
	 * Coordonnée y du noeud.
	*/

	protected int y;

	/**
	 * tableau du noeud actuel.
	*/
    	protected Board tableau;

	/**
	 * Noeud parent.
	*/
	protected Noeud parent; 

	/**
	 * Le coût du chemin du noeud de départ à ce noeud.
	*/
	protected int heuristiqueCout;

	/**
	 * Le nombre de coût du noeud de départ à ce noeud.
	*/
	protected int nbCout;

	/**
	 * Le coût du chemin le moins cher du noeud courant au noeud but.
	*/
	protected int finalCout;

	/**
	 * Construit un noeud
	 * @param x Coordonnée x du noeud
	 * @param y Coordonnée y du noeud
	 * @param tableau l'état du jeu
	 */
	
	public Noeud(int x, int y, Board tableau)
	{
		this.x=x;
		this.y=y;
		this.heuristiqueCout=0;
		this.finalCout=0;
		this.tableau=tableau;
		this.nbCout= 0;
	}

	/**
	 * Construit un noeud
	 * @param x Coordonnée x du noeud
	 * @param y Coordonnée y du noeud
	 * @param heuristiqueCout Le coût du chemin du noeud de départ à ce noeud
	 * @param finalCout Le coût du chemin le moins cher du noeud courant au noeud but.
	 * @param nbCout Le nombre de coût du noeud de départ à ce noeud.
	 * @param tableau l'état du jeu
	 */

		public Noeud(Noeud parent, int x, int y, int heuristiqueCout, int finalCout, int nbCout, Board tableau) 
	{
		this.parent = parent;
		this.x = x;
		this.y = y;
		this.heuristiqueCout=heuristiqueCout;
		this.finalCout=finalCout;
		this.tableau=tableau;
		this.nbCout= nbCout;
	}

	/**
	 * Accesseur
	 * @return La coordonnée x du noeud
	*/
	
	public int getX()
	{
		return x;
	}

	/**
	 * Accesseur
	 * @return La coordonnée y du noeud
	*/

	public int getY()
	{
		return y;
	}

	/**
	 * Accesseur
	 * @return Le noeud parent
	*/

	public Noeud getParent()
	{
		return parent;
	}

	/**
	 * Accesseur
	 * @return getheuristiqueCout
	*/

	public int getheuristiqueCout()
	{
		return heuristiqueCout;
	}

	/**
	 * Accesseur
	 * @return getfinalCout
	*/

	public int getfinalCout()
	{
		return finalCout;
	}

	/**
	 * Accesseur
	 * @return Le tableau, l'état du jeu
	*/

	public Board getBoard()
	{
		return this.tableau;
	}

	/**
	 * Méthode
	 * @return les cordoonées d'un noeud
	*/

	public String afficherNoeud() {
		return "x :"+this.getX()+" y:"+	this.getY()+" h:"+this.heuristiqueCout+" g:"+this.finalCout;
		
	}
	
	
	
}
