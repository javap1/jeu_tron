package model;
import java.util.ArrayList;
/**
 * Algorithme AlphaBeta
 * @see Algorithm
 */
public class Alphabeta implements Algorithm {

	private int alpha = -1000;
  private int beta = 1000;

  public String operation(Board game) {
		String directionBestChoice = null;
		int bestChoice = -1001;


        int x ,y;
	     ArrayList<Integer> coordonee=new ArrayList<>();
	  	String[] directions = {"N", "E", "S", "W"};

		for(String direction : directions){
			if(game.isCellMovableTo(game.currentPlayer.getX(),game.currentPlayer.getY(), direction)){
				Board clone = game.CopyBoard();
				clone.play(clone.currentPlayer.getCouleur() ,direction);
				int alphabeta = alphabeta(clone, alpha,beta , game.getProfondeur()-1, game.currentPlayer);
			//	System.out.println("direction  :" + direction + " Alphabeta  :" + alphabeta);
				if( alphabeta > bestChoice){

					bestChoice = alphabeta;
					directionBestChoice = direction;
				}
			}
		}
		return directionBestChoice;
	}

	/**
	 * AlphaBeta est un algorithme qui permet de trouver un coup optimale
	 * pour un joueur donne, en fonction d'une profondeur de recherche donnee
	 * L'algorithme est le meme que Minimax mais avec un elagage
	 * lors de la recherche du meilleurs coups possible,
	 * ce qui rend cette algorithme bien plus rapide.
	 *
	 * @see Minimax#minimax
	 * @param game une simulation du jeu
	 * @param alpha Valeur specifique a l'algorithme AlphaBeta
	 * @param beta Valeur specifique a l'algorithme AlphaBeta
	 * @param profondeur profondeur de recherche de l'algorithme
	 * @param player Le joueur qui doit jouer
	 * @return une valeur entiere representant la valeur de ce noeud
	 */
	private int alphabeta(Board game, int alpha, int beta,int profondeur, Player player){
		if(profondeur == 0){
		      // System.out.println("m :" + game.value(player));
			return game.value(player);
		}

		//si c'est un noeud joueur
		if(game.currentPlayer.getCouleur().equals(player.getCouleur())){
			for (Board gameNext : game.listsuccesseurs()) {
				//System.out.println("Alpha :" + alpha + "  profondeur :" + profondeur);
				alpha = Math.max(alpha, alphabeta(gameNext, alpha, beta, profondeur-1, player));
				if(alpha >= beta)
				//System.out.println("Alpha :" + alpha + "  profondeur :" + profondeur);
					return alpha;
			}
		//	System.out.println("Alpha :" + alpha + "  profondeur :" + profondeur);
			return alpha;
		}
		else{//si c'est un noeud opposant
			for (Board gameNext : game.listsuccesseurs()) {

				beta = Math.min(beta,alphabeta(gameNext, alpha, beta, profondeur-1, player));

				if(alpha >= beta)
					return beta;
			}
		//	System.out.println("beta :" + beta + "  profondeur  :" + profondeur);
			return beta;
		}
	}

}