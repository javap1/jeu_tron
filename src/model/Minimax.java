package model;

public class Minimax implements Algorithm{
 
    @Override
	public String operation(Board game) {
        String directionBestChoice = null;
		int bestChoice = -1001;
        String[] directions = {"N", "E", "S", "W"};

		for(String direction : directions){
			if(game.isCellMovableTo(game.currentPlayer.getX(),game.currentPlayer.getY(), direction)){
				//nodeCount++;
				Board clone = game.CopyBoard();
				clone.play(clone.currentPlayer.getCouleur() ,direction);
				int minimax = minimax(clone, game.getProfondeur()-1, game.currentPlayer);
                               System.out.println("direction "+ direction + "  minmax : "+ minimax + " player"+game.currentPlayer.getCouleur() );
				if( minimax > bestChoice){
					bestChoice = minimax;
					directionBestChoice = direction;
				}
			}
		}

        return directionBestChoice;
    }
    /**
	 * L'algorithme Minimax est un algorithme permettant de connaitre le coup optimal d'un joueur dans une situation a deux joueurs
	 * @param game une simulation du jeu
	 * @param profondeur profondeur de recherche de l'algorithme
	 * @param player Le joueur qui doit jouer
	 * @return une valeur entiere representant la valeur de ce noeud
	 */
	private int minimax(Board game, int profondeur, Player player){
		int m = 0;
		if(profondeur == 0){
                        //System.out.println("  m0 : "+ game.value(player));
			return game.value(player);
		}
		
		if(game.currentPlayer.getCouleur().equals(player.getCouleur())){
			m = - 1000;
			for (Board gameNext : game.listsuccesseurs()) {
				//nodeCount++;
				int tmp = minimax(gameNext, profondeur - 1, player);
				m = Math.max(m, tmp);
			}
		}
		else{//si c'est un noeud opposant
			m = 1000;
			for (Board gameNext : game.listsuccesseurs()) {
				//nodeCount++;
				int tmp = minimax(gameNext, profondeur - 1, player);
				m = Math.min(m, tmp);
			}
		}
               // System.out.println("  m : "+ m + " profondeur" + profondeur);
		return m;
	}
    
}
