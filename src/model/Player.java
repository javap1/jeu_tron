package model;


public class Player
{
    
  /**
	 * La coordonnée x du player
	*/
	protected int x;

	/**
	 * La coordonnée y du player
	*/
	protected int y;

	/**
	 * La couleur du player
	*/
	protected String couleur;
	
    protected boolean perdu ;

	private boolean firstPlayer;
	
	/**
	 * Contruction du robot
	 * @param couleur : couleur du robot
	*/
	public Player(String couleur)
	{
		this.couleur=couleur;
		this.perdu = false;
		this.firstPlayer=false;
	}

	
	
	/**
	 * Contruction du robot
	 * @param x coordonné x du robot
	 * @param y coordonné y du robot
	 * @param couleur : couleur du robot
	*/

	public Player(int x,int y,String couleur)
	{
		this.x=x;
		this.y=y;
		this.couleur=couleur;
		this.firstPlayer=false;
	}

	/**
	 * Accesseur
	 * @return La coordonnée x du player
	*/
	public int getX(){
		return x;
	}

	public void setX(int xx){
		this.x=xx;
	}


	/**
	 * Accesseur
	 * @return La coordonnée y du player
	*/
	public int getY(){
		return y;
	}

	public void setY(int yy){
		this.y=yy;
	}

    public void setCouleur(String c)
    {
        this.couleur=c;
    }
    
	public String getCouleur()
	{
		return this.couleur;
	}
	public void setPerdu(boolean p)
    {
        this.perdu=p;
    }
    
	public boolean getPerdu()
	{
		return this.perdu;
	}

	/**
	 * Définis le joueur comme étant premier joueur du jeu
	 */
	public void setFirst(){
		this.firstPlayer = true;
	}

	public void setFirst(boolean b){
		this.firstPlayer = b;
	}
	
	/**
	 * Vérifie si le joueur est premier joueur du jeu
	 * @return
	 */
	public boolean isFirst(){
		return this.firstPlayer;
	}

	/**
	 * Méthode
	 * @return Les coordonnées d'un player
	*/
	public String afficherPlayer()
	{
		return "("+this.x+","+this.y+")"+ " " +"couleur: "+this.couleur + "perdu :" + this.perdu ;
	} 

}