package model;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Demo
{
    static final Scanner scanner = new Scanner(System.in);
    
    public static void main (String [] arg)
	{       
                System.out.println("Welcome to Game of Tron");
                
                Algorithm algo = choixAlgo();
                
                Board game =choixGrille(algo);
                
                game.afficherGrid();
                
                 
                System.out.println("Do you want to save the game ? (y/n)");
                playGame(scanner.nextLine().equals("y"),game,algo);

                
        }
    
    public static Board choixGrille(Algorithm algo){
            
            
        int nmbr=2 ,profondeur,profocoalition=0;
        System.out.println("how much players do u want ?");
        String p ;
           nmbr= Integer.parseInt(scanner.nextLine());
           while(nmbr<2 ){
               System.out.println(" enter a number of players > 2  please !");
               nmbr= Integer.parseInt(scanner.nextLine());
            };
                    
        Board game = new Board(nmbr);
        
       if(algo instanceof Paranoid) {
           
         
            
           System.out.println("whats is the depth of researsh that u want for the first team ? (profondeur)");
           profondeur = Integer.parseInt(scanner.nextLine());
           
           System.out.println(" Choose first team " );
            System.out.println(" number of players : " );
            for(int i=1;i<=nmbr;i++)
               System.out.print(i +"\t") ;
           int i=0;
           do{ 
                System.out.println("enter number of the player to add to the first team : " );
                p=scanner.nextLine();
               // System.out.println(game.getPlayer2(p).getCouleur());
                while(game.getPlayer2(p).getCouleur().equals("H") ){
                   System.out.println(" enter a correct number please !");
                   p=scanner.nextLine();
                };
                game.getPlayer2(p).setFirst();
                i++;
                System.out.println("Do you want to add another player ? (y/n)");
           }while(scanner.nextLine().equals("y") && i<nmbr-1);
           
           System.out.println("the rest of players will be in the second team");
           
           System.out.println("whats is the depth of researsh that u want for the second team ? (profondeur)");
           profocoalition=Integer.parseInt(scanner.nextLine());
           
           
                       
            
         
       }
       else 
       {   
           System.out.println("whats is the depth of researsh that u want ? (profondeur)");
           profondeur = Integer.parseInt(scanner.nextLine());
       
       }       
       
       game.setProfondeur(profondeur);
       game.setProfondeurCoalition(profocoalition);
      
       game.placerPlayerinit(placementPlayers(nmbr,game.getHeight(),game.getWeight()));
       
       return game;
            
            
      }
    
    
    
    
    
    
    public static Algorithm choixAlgo(){
       
       System.out.println("Enter the number of Algorithm that u want to use : \n1 --> MinMAX \n2 --> Alphabeta \n3 --> Paranoid \n");
       int nmbr=0;
       try{
       nmbr = Integer.parseInt(scanner.nextLine());
       }catch( NumberFormatException err){
          System.out.println(err + "u should enter a number" );
       };
       
       while( nmbr>3 || nmbr<=0){
       System.out.println("enter a correct number");
       nmbr = Integer.parseInt(scanner.nextLine());
       };
       
       switch(nmbr){
   
       case 1: 
           return new Minimax();
   
       case 2:
           return new Alphabeta();
   
       case 3:
           return new Paranoid();
        default:
           return null;
   }
        
        
    }
    
        
    
    public static void playGame(boolean save,Board game,Algorithm algo)
    {      
            game.nextPlayer();
            
            
            
            
            
            //game.currentPlayer.setFirst();
            
            
            
            System.out.println("First player: " + game.currentPlayer.getCouleur());
            if(game.getProfondeurCoalition()!=0){
            System.out.println("Depth of the first team : " + game.getProfondeur());
            System.out.println("Depth of second team : " + game.getProfondeurCoalition());}
            else 
               System.out.println("Depth : " + game.getProfondeur());
            
            game.afficherGrid();
            System.out.println(" player:" + game.currentPlayer.getCouleur());
           
            
            
            if(save)
                sauvegardeJeu(game,algo);
            else
                JeusansSauvegarde(game,algo);
    
    }
    
 
    
    
    public static void JeusansSauvegarde(Board game,Algorithm algo ){
            
            String direction="n";
            Player p;
           // Algorithm algo1 = new Minimax();
            //game.nextPlayer();
            long startTime = System.currentTimeMillis();
            do{
                //game.isDead();
               // if(game.currentPlayer.getCouleur().equals("2"))
               // direction = algo1.operation(game);
               // if(game.currentPlayer.getCouleur().equals("1"))
                direction = algo.operation(game);
                p=game.getPlayer2(game.currentPlayer.getCouleur());
                game.play(game.currentPlayer.getCouleur(),direction);
               // game.afficherGrid();
               // System.out.println("le joueur de cet iteration " + p.getCouleur());
                game.isDead(p);
             
            }while(!game.terminal());
         long endTime = System.currentTimeMillis();
        System.out.println("le temps du jeu est : "+(endTime-startTime));
    }
    
public static void sauvegardeJeu(Board game,Algorithm algo){
            BufferedWriter writer =null;
        
            try {
            
            writer = new BufferedWriter(new FileWriter("grilles.txt"));
            String direction;
             Player p;
            do{
                //game.isDead();
                p=game.getPlayer2(game.currentPlayer.getCouleur());
                direction = algo.operation(game);
                game.play(game.currentPlayer.getCouleur(),direction);
                game.writeGrid(writer);
                game.isDead(p);
            }while(!game.terminal());
            
            } catch (IOException ex) {
                 ex.printStackTrace();
            } finally {
                try {
                    writer.close();
                } catch (IOException ex) {
                  ex.printStackTrace();  
                }
            }
    }

    private static int[] placementPlayers(int nmbrPlayers, int h, int w) {
        System.out.println("Players position in the board");
        int[] cord = new int[nmbrPlayers*2];
        int x=-1,y=-1,i=0,j=0;
        
        while(i<nmbrPlayers && j<nmbrPlayers*2)
        {       System.out.println("enter a position for player "+(i+1)+"\n");
                try{   System.out.println("x"+(i+1)+"=");
                       x = Integer.parseInt(scanner.nextLine());
                        System.out.println("y"+(i+1)+"=");
                       y= Integer.parseInt(scanner.nextLine());

                    }catch( NumberFormatException err){
                           System.out.println("\n" +err + "u should enter a number not a String" );
                        };

                while(x>=h-1 || x<1 || y>=w-1 || y<1 || inTab(cord,x,y,j) ){
                  try{ 
                    System.out.println("\n enter a correct number between 1  and "+ (h-1)+" for x"+(i+1)+"\n x"+(i+1)+"=");
                    x = Integer.parseInt(scanner.nextLine());
                    System.out.println("\n enter a correct number between 1 and "+ (w-1)+" for y"+(i+1)+"\n y"+(i+1)+"=");
                    y= Integer.parseInt(scanner.nextLine());
                    }catch( NumberFormatException err){
                           System.out.println("\n" +err + "u should enter a number not a String" );
                        };
                }
                
                cord[j++]=x;
		cord[j++]=y;
                i++;    
                
        };
    return cord;
    }
    
    
  public static boolean inTab(int[] tab , int x , int y,int j){
      
          for(int i=0;i<j;i+=2)
          { 
              if(tab[i]==x && tab[i+1]==y)
                  return true;
          
          }
           
          return false;
  
  
  }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
} 
    
    
    
    
    
    
    
    
    

