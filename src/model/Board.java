package model;
import java.util.ArrayList;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Board
{
    protected String[][] grille;

    private int profondeurCoalition;
    private int profondeur;
    public Player currentPlayer ;
    protected ArrayList<Player> players = new ArrayList<>();
    public int height;
    public int weight;
    protected int nbPlayers;
    private int nextPlayerIdx;

    /**
	 * Construction du Board
	 * sans paramètre (par defaut)
     * @param nbPlayers
	 */

	public Board(int nbPlayers)
	{   
               this.nbPlayers = nbPlayers;
		
               if(nbPlayers<5)
		 {this.height = 20;
		  this.weight =20;}
		else 
		  {this.height = 5*nbPlayers;
		  this.weight =5*nbPlayers;}
		
                
		this.grille=grille();

               for(int i=1;i<nbPlayers+1;i++)
		  players.add(new Player(""+i));
        
		this.nextPlayerIdx = 0;
        
		//placerPlayerinit();

		
	}
    

	
        
    public void nextPlayer(){
		if(this.nextPlayerIdx >= this.players.size()){
			this.nextPlayerIdx = 0;
		}
		this.currentPlayer = this.players.get(this.nextPlayerIdx++);
	}
	

    /**
	 * Méthode
	 * @return la grille (plateau du jeu)
	 */	
	
    public String[][] getGrille()
	{
		return this.grille;
	}
	
   /**
	 * Méthode  
	 * @return une grille vide de milieu (V) et entourée par des murs(M) 
	 */
   public final String[][] grille()
    {   String[][] grille1 = new String[this.height][this.weight];
		for (int i=0;i<this.height;i++)
		{
			for(int j=0;j<this.weight;j++)
			{
				if(i==0 || j==0 || i==this.height-1 || j==this.weight-1)
				 grille1[i][j]="M";
				else 
				 grille1[i][j]="V";
			}
		}
        return grille1;
    }
   
   /**
	 * Méthode  
	 * Affiche le plateau de jeu 
	 */
   public void afficherGrid()
	{       
             
            

		this.grille=getGrille();
		int size = 5;
		String delimiter = " | ";
                
		System.out.print("     ");
		for(int i=0;i<this.height;i++)
		{
			String line = String.format("%1$"+size+ "s", Integer.toString(i));
                        System.out.print(delimiter + line +" ");
		}
		System.out.println(delimiter);
		System.out.print("-----");
		for(int i=0;i<this.height;i++)
		{
			String line = String.format("%1$"+size+ "s", "+-----");
                        System.out.print("-" + line + "--");
		}
		System.out.println(delimiter);

		for(int i=0;i<this.height;i++)
		{       
			System.out.print(String.format("%1$"+size+ "s", Integer.toString(i) + "|"));
			for(int j=0;j<this.height;j++)
			{	
				String res = this.grille[i][j];
				if(res.equals("V"))
					res = " ";
				String line = String.format("%1$"+size+ "s", res);
				System.out.print(delimiter + line +" ");
				//System.out.print(delimiter+this.grille[i][j]);
			}
			System.out.println(delimiter);
		}
                
                  
	}

   public void writeGrid(BufferedWriter writer)
	{       
              try {
               writer.write("\n");
            

		this.grille=getGrille();
		int size = 5;
		String delimiter = " | ";
                writer.write("     ");
		System.out.print("     ");
		for(int i=0;i<this.height;i++)
		{
			String line = String.format("%1$"+size+ "s", Integer.toString(i));
			writer.write(delimiter + line +" ");
                        System.out.print(delimiter + line +" ");
		}
                writer.write(delimiter +"\n");
		System.out.println(delimiter);
                writer.write("-----");
		System.out.print("-----");
		for(int i=0;i<this.height;i++)
		{
			String line = String.format("%1$"+size+ "s", "+-----");
			writer.write("-" + line + "--");
                        System.out.print("-" + line + "--");
		}
                writer.write(delimiter +"\n");
		System.out.println(delimiter);

		for(int i=0;i<this.height;i++)
		{       
                        writer.write(String.format("%1$"+size+ "s", Integer.toString(i) + "|"));
			System.out.print(String.format("%1$"+size+ "s", Integer.toString(i) + "|"));
			for(int j=0;j<this.height;j++)
			{	
				String res = this.grille[i][j];
				if(res.equals("V"))
					res = " ";
				String line = String.format("%1$"+size+ "s", res);
                                writer.write(delimiter + line +" ");
				System.out.print(delimiter + line +" ");
				//System.out.print(delimiter+this.grille[i][j]);
			}
                        writer.write(delimiter+"\n");                        
			System.out.println(delimiter);
		}
                
                //writer.close();
                  } catch (IOException ex) {
               ex.printStackTrace();
              }
	}


	public void afficherHeuristique(int[][] grille )
	{
		
		int size = 5;
		String delimiter = " | ";
		System.out.print("     ");
		for(int i=0;i<this.height;i++)
		{
			String line = String.format("%1$"+size+ "s", Integer.toString(i));
			System.out.print(delimiter + line +" ");
		}
		
		System.out.println(delimiter);
		System.out.print("-----");
		for(int i=0;i<this.height;i++)
		{
			String line = String.format("%1$"+size+ "s", "+-----");
			System.out.print("-" + line + "--");
		}
		System.out.println(delimiter);

		for(int i=0;i<this.height;i++)
		{
			System.out.print(String.format("%1$"+size+ "s", Integer.toString(i) + "|"));
			for(int j=0;j<this.height;j++)
			{	
				String res = String.valueOf(grille[i][j]);
				if(res.equals("V"))
					res = " ";
				String line = String.format("%1$"+size+ "s", res);
				System.out.print(delimiter + line +" ");
				//System.out.print(delimiter+this.grille[i][j]);
			}
			System.out.println(delimiter);
		}
	}
	
        
        
        public void placerPlayerinit(int[] tabCord)
	{
		int i=0;
		for(Player p : this.players)
		{       
                       
			p.setX(tabCord[i++]);
			p.setY(tabCord[i++]);
                        
			System.out.println(p.getCouleur() + "("+p.x+" ,"+p.y+")");
			
			this.grille[p.getX()][p.getY()]=p.getCouleur();
		}
	}
        
        
	 /**
	 * Méthode  
	 * placer aléatoirement les joueurs dans le plateau de jeu
	 */
	public void placerPlayerinit()
	{
		Random random = new Random();
	
		int x,y;
		for(Player p : this.players)
		{
			do {x= random.nextInt(this.height);
			    y= random.nextInt(this.weight);
			    } while ( !"V".equals(this.grille[x][y]));
			
			
			//System.out.println("("+x+" ,"+y+")");
			p.x=x;
			p.y=y;
			//System.out.println(p.getCouleur() + "("+p.x+" ,"+p.y+")");
			
			this.grille[x][y]=p.getCouleur();
		}
	}
	
	/**
	 * Méthode  
	 * savoir si on peut deplacer un joueur avec les cordonnées (x,y) dans la direction souhaitée
	 */
	public boolean isCellMovableTo(  int x, int y, String direction) {
		switch(direction) {
			case "N":
				return this.grille[x-1][y].contains("V");
			case "S":
				return this.grille[x+1][y].contains("V");
			case "E":
				return this.grille[x][y+1].contains("V");
			case "W":
				return this.grille[x][y-1].contains("V");
			default:
				return false;
		}
	}
	
	/**
	 * Méthode
	 * @param x coordonnée x du case
	 * @param y coordonnée y du case
	 * @param direction la direction du robot {droit, gauche, haut, bas}
	 * @return liste contenant les coordonnées x, y d'où le robot va se stopper
	 */
	
	public ArrayList<Integer> posArrive(int x,int y,String direction)
	{
		ArrayList<Integer> pos =new ArrayList<>();
		
		
			if (direction == "N" )
			    x--;
			if (direction == "E" )
			    y++;
			if (direction == "S" ) 
			    x++;
			if (direction == "W" ) 
				y--;
			 
		
		pos.add(x);
		pos.add(y);
		return pos;
		
	}
	/**
	 * Méthode
	 * @return une copy du plateau de jeu 
	 */

	 public String[][] CopyGrille()
	 {
		 String[][] d=new String[this.height][this.weight];
		 //this.grille=this.grille();
		 for(int i=0;i<this.height;i++)
			for(int j=0;j<this.weight;j++)
			   d[i][j]=this.grille[i][j];
		 
		 return d;		 
	 }
   
    /**
	 * Méthode
	 *Avec CopyGrille: @return une copy du plateau de jeu avec le mouvement demandé bien fait
	  Sans CopyGrille : change dans le plateau initial directement 
     * @param couleur
     * @param direction
	 */
	public void play(String couleur,String direction)
	{   
		String[][] d=this.getGrille();
	    if(direction == null){
			this.players.remove(this.currentPlayer);
			this.nextPlayer();
			return;
		}
        
		//String[][] d = CopyGrille();
	    Player activeR= getPlayer2(couleur);
		//System.out.println(activeR.x+","+activeR.y);
        //if(!activeR.getPerdu())
		  //System.out.println("le joueur "+ couleur + " a perdu ");
		//else
		 //{
				ArrayList<Integer> coordonee=new ArrayList<>();
				
				coordonee=this.posArrive(activeR.x,activeR.y,direction);
				
				//System.out.println(activeR.x+","+activeR.y);
		
				int x = activeR.x;
				int y =activeR.y;

				
		        
                //if(!activeR.getPerdu())
                 // System.out.println("le joueur "+ couleur + " a perdu " + "Move : " + "("+ couleur + ","+ direction + ")");
				//else
				//{
						
							

						
							d[activeR.x][activeR.y]+="M";

							activeR.x=coordonee.get(0);
							activeR.y=coordonee.get(1);
							
							d[coordonee.get(0)][coordonee.get(1)]= activeR.getCouleur();
						
						
				    //}
			//}
		this.nextPlayer();
		
	}
	/**
	 * Méthode
	 * @param r un player
	 * @return un robot 'une copie d'un player'
	 */	
	
	 public Player CopyPlayer(Player p)
	 {
			Player nr=new Player(p.getCouleur());
			nr.setX(p.getX());
			nr.setY(p.getY());
			nr.setFirst(p.isFirst());
			return nr;
	 }
    

	public int getnextPlayerIdx()
     {
		return nextPlayerIdx;
	 }


	/**
	 * Méthode
	 * @return board 'une copie de board'
	 */	 
	 
	public Board CopyBoard()
	{   
		Board b = new Board(this.nbPlayers);
		b.currentPlayer = CopyPlayer(this.currentPlayer);
		b.grille = CopyGrille();
		b.setProfondeurCoalition(getProfondeurCoalition());
        b.setProfondeur(getProfondeur());
		b.players = new ArrayList<>(); 
		for(Player p : this.players )
		  b.players.add(CopyPlayer(p));
        
	    b.nextPlayerIdx = this.getnextPlayerIdx();
		return b;
	}
	/**
	 * Méthode
	 * @param d plateau de jeu
	 * @param p un player
	 * @param x coordonnée x du case
	 * @param y coordonnée y du case
	 * @param x1 coordonnée x1 du case
	 * @param y1 coordonnée y1 du case
	 * @return le plateau de jeu dans une nouvelle état
	 */	
	
	 public String[][] playN(String[][] d,Player p,int x,int y,int x1,int y1)
	 {  
		
		 d[x1][y1]=p.getCouleur();
		 
		 return d;
	 }
	 
	 
	 public Player getPlayer1(String couleur)
	 {
		Player p =new Player("H");
		for (int i=0;i<this.height;i++)
		{
			for(int j=0;j<this.weight;j++)
			{
				if(this.grille[i][j].equals(couleur))
				{
					p.setCouleur(couleur);
					p.x=i;
					p.y=j;
				}
			}
		}
		return p;
	 }


	public Player getPlayer2(String couleur)
	 { 
		Player p1 =new Player("H");
	    for (Player p : this.players)
		   if(p.getCouleur().equals(couleur))
		      return p;
		
		
		return p1;
	 }
    
	/**
	 * Heuristique utilisee par les IAs
	 * @param player le joueurs qui doit etre evalues
	 * @return une valeur entiere representant l'avantage du joueur
	 */
	int value(Player player){
		int value = 0;
		
		for (Player p : this.players) {
			if(p.getCouleur().equals(player.getCouleur()))
				value = this.domainCount(p);
		}
		return value;
	}
	
	/**
	 * Retourne le nombre de cases atteintes par le joueur passe en parametre
	 * @param player le joueur a evaluer
	 * @return le nombre de case atteintes par le joueur passe en parametre
	 */
	private int domainCount(Player player){
		int positionCount = 0;
		int[][] position = new int[this.height][this.weight];
		int[][] positionToMatch = new int[this.height][this.weight];
		
		for (int row = 0; row < this.height; row++) {
			for (int col = 0; col < this.weight; col++) {
				position[row][col] = -1;
				positionToMatch[row][col] = -1;
			}
		}
		
		for (int i = 0; i < players.size(); i++) {
			position = dijkstra(position, 0, players.get(i).getX(), players.get(i).getY());
			//afficherHeuristique(position);
		}
		//System.out.println(" position   ");
                //afficherHeuristique(position);
               // System.out.println(" position to match  ");
		positionToMatch = dijkstra(positionToMatch, 0, player.getX(), player.getY());
		//afficherHeuristique(positionToMatch);

		for (int row = 0; row < this.height; row++) {
			for (int col = 0; col < this.weight; col++) {
				if(position[row][col] != -1 && positionToMatch[row][col] != 0){
					if(position[row][col] == positionToMatch[row][col]){
						positionCount++;
					}
				}
			}
		}
		return positionCount;
	}
	
	/**
	 * Verifie si les valeurs x et y entres en parametres representent une valeur correcte
	 * @param row la ligne 
	 * @param col la colonne
	 * @return vrai si [row][col] appartient au tableau de jeu sinon faux
	 */
	private boolean isInBoard(int row, int col){
		return row >= 0 && row < this.height && col >= 0 && col < this.weight;
	}
	
	/**
	 * Effectue un calcul du plus court chemin entre chaque joueur et chaque case
	 * @param positions Un tableau d'entier de la meme taille que le plateau de jeu
	 * @param iteration le nombre d'iterations de l'algos
	 * @param row la ligne representant une des deux valeurs du curseur
	 * @param col la colonne representant une des deux valeurs du curseur
	 * @return un tableau d'entier representant les cases atteintes pour chaques joueurs 
	 */
	private int[][] dijkstra(int[][] positions, int iteration, int row, int col){
		if(isInBoard(row, col)){
			if(this.grille[row][col].contains("M")){
				//System.out.println("m Ouuuuuuuuuuuuuut");
				return positions;
			}
			else{
				if(positions[row][col] > iteration || positions[row][col] == -1){
					positions[row][col] = iteration;
					
				}
			}
		}
		int x ,y;
	    ArrayList<Integer> coordonee=new ArrayList<>();
		String[] directions = {"N", "E", "S", "W"};
		for (String direction : directions) {
			coordonee = posArrive(row,col,direction);
			if(isInBoard(coordonee.get(0), coordonee.get(1))){
				if(positions[coordonee.get(0)][coordonee.get(1)] > iteration + 1 || positions[coordonee.get(0)][coordonee.get(1)] == -1)
					positions = dijkstra(positions, iteration + 1, coordonee.get(0), coordonee.get(1));
			}
		}
		return positions;
	}
    
/**
	 * Getter de la profondeur utilisee par les IAs de la coalition
	 * @return la profondeur utilisee par les IAs de la coalition
	 */
	public int getProfondeurCoalition() {
		return profondeurCoalition;
	}

	/**
	 * Setter de la profondeur utilisee par les IAs de la coalition
	 * @param profondeurCoalition la profondeur utilisee par les IAs de la coalition
	 */
	public void setProfondeurCoalition(int profondeurCoalition) {
		this.profondeurCoalition = profondeurCoalition;
	}

	/**
	 * Getter de la profondeur utilisee par les IAs
	 * @return la profondeur utilisee par les IAs
	 */
	public int getProfondeur() {
		return profondeur;
	}

	/**
	 * Setter de la profondeur utilisee par les IAs
	 * @param profondeur la profondeur utilisee par les IAs
	 */
	public void setProfondeur(int profondeur) {
		this.profondeur = profondeur;
	}

	/**
	 * Retourne les Etats successeurs de la parties qui peuvent etre compris entre 0 et 3
	 * @return Les etats successeurs de la parties
	 */
	public Board[] listsuccesseurs(){
		
		ArrayList<Board> coupSuivant;
		coupSuivant = new ArrayList<Board>();

		int x ,y;
	    ArrayList<Integer> coordonee=new ArrayList<>();
		String[] directions = {"N", "E", "S", "W"};
		for (String direction : directions) {
			if(this.isCellMovableTo(this.currentPlayer.getX(),this.currentPlayer.getY(), direction)){
				Board clone = this.CopyBoard();
				clone.play(this.currentPlayer.getCouleur() , direction);
				coupSuivant.add(clone);	
			}
		}
			
		Board[] games = new Board[coupSuivant.size()];
		return coupSuivant.toArray(games);
	}
	
	
	
	/**
	 * Verifie si le joueur courrant est mort et le supprime
	 * @return true si le joueur actuelle ne peut plus bouger
	 */
	public boolean isDead() {
		int nbCasesinaccessible=0;
		//boolean zeroDirection = true;
		String[] directions = {"N", "E", "S", "W"};
		for (String direction : directions) {
			//System.out.println(this.currentPlayer.afficherPlayer());
            // System.out.println( this.currentPlayer.getCouleur() + " is movable to " +direction +  ": "+ this.isCellMovableTo(this.currentPlayer.getX(),this.currentPlayer.getY(), direction) );
			if(!this.isCellMovableTo(this.currentPlayer.getX(),this.currentPlayer.getY(), direction)){
			   nbCasesinaccessible++;
			
		    }
		}

		if(nbCasesinaccessible == 4){
			System.out.println("Player " + this.currentPlayer.getCouleur() + " loose");
			players.remove(this.currentPlayer);
		}
		
		return nbCasesinaccessible == 4;
	}
        
        /**
	 * Verifie si un joueur  est mort et le supprime
	 * @return true si le joueur actuelle ne peut plus bouger
	 */
        public boolean isDead(Player p) {
		int nbCasesinaccessible=0;
		//boolean zeroDirection = true;
		String[] directions = {"N", "E", "S", "W"};
		for (String direction : directions) {
			//System.out.println(this.currentPlayer.afficherPlayer());
               // System.out.println( p.getCouleur() + " is movable to " +direction +  ": "+ this.isCellMovableTo(p.getX(),p.getY(), direction) );
			if(!this.isCellMovableTo(p.getX(),p.getY(), direction)){
			   nbCasesinaccessible++;
			
		    }
		}

		if(nbCasesinaccessible == 4){
			System.out.println("Player " + p.getCouleur() + " loose");  
			players.remove(p);
		}
		
		return nbCasesinaccessible == 4;
	}
    
	/**
	 * Verifie si la partie est terminee (si il ne reste qu'un joueur)
	 * @return true si la partie est terminee
	 */
	public boolean terminal() {
			if (players.size() == 1) {
				System.out.println("Player " + this.players.get(0).getCouleur() + " win");
				return true;
			} else 
				return false;
	}

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public int getNbPlayers() {
        return nbPlayers;
    }


     


}



