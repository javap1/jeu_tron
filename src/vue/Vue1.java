package vue;

import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.Board;

public class Vue1 extends JFrame implements ActionListener  {
    JButton btn;
    JComboBox com;
    JTextField textPro;
    JTextField textPro2;
    public String pro2="";
    public Vue1()
    {
        JPanel p1=new JPanel();
        JLabel titre=new JLabel("Bienvenue");
        titre.setFont(new Font("Serif", Font.BOLD, 50));
        p1.add(titre);
        //getContentPane().add(p1, BorderLayout.NORTH);
        JPanel container = new JPanel();
        JLabel l=new JLabel("choisir l'algorithme");
        container.add(l);
        String [] list={"Paranoid","MinMax","Alphabeta"};
        com=new JComboBox(list);
        container.add(com);
        //getContentPane().add(container, BorderLayout.CENTER);
        JPanel p2=new JPanel();
        JLabel l1=new JLabel("choisir le nombre de joueur");
        p2.add(l1);
        String [] listJ={"2","3","4","5","6"};
        JComboBox comJ=new JComboBox(listJ);
        p2.add(comJ);
        JLabel labelProf2=new JLabel("La deuxième profondeur ");
        textPro2=new JTextField("Entrer la deuxième profondeur !!");
        //getContentPane().add(container, BorderLayout.SOUTH);
        JPanel partieButton =new JPanel();
        btn =new JButton("Start");
        btn.setSize(20,50);
        JPanel contenu = new JPanel();
        JPanel partieProf2=new JPanel();
        com.addActionListener(new ActionListener() {     
            @Override
            public void actionPerformed(ActionEvent e) {
                if(com.getSelectedItem().toString().equals("Paranoid"))
                {
                    partieProf2.add(labelProf2);
                    partieProf2.add(textPro2);
                    

                }      
            }
          });
        JLabel labelProf=new JLabel("La profondeur");
        textPro=new JTextField("Entrer la profondeur !!");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nbj=comJ.getSelectedItem().toString();
                String algorithm=com.getSelectedItem().toString();
                String prof1=textPro.getText()+"";
                String prof2="0";
                if(textPro2.getText().equals("Entrer la deuxième profondeur !"))
                {
                    prof2=textPro2.getText()+"";

                }
                Board b=new Board(Integer.parseInt(nbj));
                b.placerPlayerinit();
                Vue1.this.setVisible(false);
                Vue v1=new Vue(b,algorithm,prof1,prof2);
            }
        });
        JPanel partieProf=new JPanel();
        partieButton.add(btn);
        contenu.setLayout(new BoxLayout(contenu, BoxLayout.Y_AXIS));
        contenu.add(p1);
        contenu.add(container);
        contenu.add(p2);
        partieProf.add(labelProf);
        partieProf.add(textPro);
        contenu.add(partieProf);
        contenu.add(partieProf2);
        contenu.add(partieButton);
        getContentPane().add(contenu);
        contenu.setBackground(Color.BLUE);

        // Configurer la fenêtre
        
        this.setTitle("Jeu de thron");
        this.setSize(900,800);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
    
}
