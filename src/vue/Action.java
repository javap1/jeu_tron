package vue;

import java.awt.*;
import javax.swing.*;

import java.awt.image.BufferedImage;

import model.*;
import vue.Vue;

public class Action extends JPanel  {

    private JButton bouton;
    private JButton caseSuivante;
    private Board b;
    private Vue vue;

    public Action(Board b, Vue vue){
        super(new GridLayout(9,1));
        this.b = b;
        this.vue = vue;

        this.add(new JLabel("Lancement de la resolution :"));
        this.bouton = new JButton("Resoudre");
        this.bouton.setEnabled(true);
        this.add(this.bouton);


        this.add(new JLabel("Visionner parcours :"));
        this.caseSuivante = new JButton(">");
        this.caseSuivante.setEnabled(false);
        this.add(this.caseSuivante);
    }

    /**
     * @return le bouton permettant de lancer la résolution avec A*
     */
    public JButton getBouton(){
        return bouton;
    }

    /**
     * @return le bouton caseSuivante
     */
    public JButton getCaseSuivante() {
        return caseSuivante;
    }
}