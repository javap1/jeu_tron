package vue;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Observable;
import java.util.Observer;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import javax.swing.border.Border;

import model.*;

public class Plateau extends JPanel implements Observer{
    public JLabel[][] plateau;
    public String[][] grille=new String[20][20];
    private Board b;
    //private Board b=new Boa;
    /**
     * @param b
     */
    public Plateau(Board b){
        super();
        this.b=b;
        this.plateau = new JLabel[b.getHeight()][b.getWeight()];
        this.setLayout(new GridLayout(b.getHeight(),b.getWeight()));
        b.afficherGrid();
        
        grille = b.CopyGrille();
        
		//b.afficherGrille();
        for(int i=0; i<b.getHeight(); i++){
            for(int j=0; j<b.getWeight(); j++){
                JLabel cellule = new JLabel();
                // On paramètre l'affichage initiale d'une cellule
                cellule.setOpaque(true); 
                cellule.setBackground(new Color(229,185,120));
                Border border = BorderFactory.createLineBorder(Color.black);
                cellule.setBorder(border);
                cellule.setPreferredSize(new Dimension(40,40));
                // On dessine le contenu de la cellule en fonction des valeurs du modèle
                // Traitement effectué dans une fonction car nous aurons besoin de cette partie du code pour modifier l'affichage de la cible une fois qu'elle a été tirée.
                //System.out.print(grille[0][grille.length-1]);
                dessiner(cellule,i,j,true);
                this.add(cellule);
                this.plateau[i][j] = cellule;
                this.repaint();           
            }
        }
    }
    public void dessiner(JLabel cellule, int i,int j, boolean alpha)
		{
        int type = alpha ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
        BufferedImage image = new BufferedImage((int)cellule.getPreferredSize().getWidth(),(int)cellule.getPreferredSize().getHeight(),type);
        Graphics2D g = image.createGraphics();
        if(i==0 && j==grille.length-1)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect(0, 0, (int)cellule.getPreferredSize().getWidth(), 5);
            g.fillRect((int)cellule.getPreferredSize().getWidth()-6, 0, 5, (int)cellule.getPreferredSize().getHeight());
            g.dispose();
        }
        if(i==0 && j==0)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect(0, 0, (int)cellule.getPreferredSize().getWidth(), 5);
            g.fillRect(0, 0, 5, (int)cellule.getPreferredSize().getHeight());
            g.dispose();
            cellule.setIcon(new ImageIcon(image));
        }
        if(i==grille.length-1 && j==0)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect(0,(int)cellule.getPreferredSize().getHeight()-6, (int)cellule.getPreferredSize().getWidth(), 5);
            g.fillRect(0, 0, 5, (int)cellule.getPreferredSize().getHeight());
            g.dispose();
            cellule.setIcon(new ImageIcon(image));
        }
        if(i==grille.length-1 && j==grille.length-1)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect(0,(int)cellule.getPreferredSize().getHeight()-6, (int)cellule.getPreferredSize().getWidth(), 5);
            g.fillRect((int)cellule.getPreferredSize().getWidth()-6, 0, 5, (int)cellule.getPreferredSize().getHeight());
            g.dispose();
            cellule.setIcon(new ImageIcon(image));
        }
        if(i==0)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect(0,0, (int)cellule.getPreferredSize().getWidth(), 5);
            g.dispose();
            cellule.setIcon(new ImageIcon(image));
        }
        
        if(j==grille.length-1)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect((int)cellule.getPreferredSize().getWidth()-6, 0, 5, (int)cellule.getPreferredSize().getHeight());
            g.dispose();
            cellule.setIcon(new ImageIcon(image));
        }
        if(j==0)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect(0, 0, 5, (int)cellule.getPreferredSize().getHeight());
            g.dispose();
            cellule.setIcon(new ImageIcon(image));
        }
        if(i==grille.length-1)
        {
            g.setColor(new Color(91,60,17));
            g.fillRect(0,(int)cellule.getPreferredSize().getHeight()-6, (int)cellule.getPreferredSize().getWidth(), 5);
            g.dispose();
            cellule.setIcon(new ImageIcon(image));
        }
        int x1=((int)cellule.getPreferredSize().getWidth()-1)/2;int x2=((int)cellule.getPreferredSize().getWidth()-10)/2;int x3=((int)cellule.getPreferredSize().getWidth())/2;int x4=((int)cellule.getPreferredSize().getWidth()-18)/2;int x5=((int)cellule.getPreferredSize().getWidth()-7)/2;int x6=((int)cellule.getPreferredSize().getWidth())/2;
        int[] x={x1,x2,x3,x4,x5,x6};
        int y1=((int)cellule.getPreferredSize().getHeight())/2;int y2=((int)cellule.getPreferredSize().getHeight()-15)/2;int y3=((int)cellule.getPreferredSize().getHeight()-20)/2;int y4=((int)cellule.getPreferredSize().getHeight())/2;int y5=((int)cellule.getPreferredSize().getHeight()-17)/2;int y6=((int)cellule.getPreferredSize().getHeight()-15)/2;
        int[] y={y1,y2,y3,y4,y5,y6};
         /* 
            On paramètre la couleur de la figure ou du player ( R=Rouge, G=Vert, B=Bleu, Y=Jaune)
        */
        if(grille[i][j].equals("1")){
            g.setColor(new Color(255,24,63));

        }
        if(grille[i][j].equals("3")){
            g.setColor(new Color(20,148,20));
        }
        if(grille[i][j].equals("2")){
            g.setColor(new Color(0,127,255));
        }
        if(grille[i][j].equals("4")){
            g.setColor(new Color(255,255,0));
        }
        if(grille[i][j].equals("1") || grille[i][j].equals("2") || grille[i][j].equals("3") || grille[i][j].equals("4"))
        {
            g.fillRect(((int)cellule.getPreferredSize().getWidth()-20)/2,((int)cellule.getPreferredSize().getHeight()-20)/2,20,20);
            cellule.setIcon(new ImageIcon(image));
        }
        if(grille[i][j].equals("1M")){
            g.setColor(new Color(255,24,63));

        }
        if(grille[i][j].equals("3M")){
            g.setColor(new Color(20,148,20));
        }
        if(grille[i][j].equals("2M")){
            g.setColor(new Color(0,127,255));
        }
        if(grille[i][j].equals("4M")){
            g.setColor(new Color(255,255,0));
        }
        if(grille[i][j].equals("1M") || grille[i][j].equals("2M") || grille[i][j].equals("3M") || grille[i][j].equals("4M"))
        {
            g.fillOval(((int)cellule.getPreferredSize().getWidth()-20)/2,((int)cellule.getPreferredSize().getHeight()-20)/2,20,20);
            cellule.setIcon(new ImageIcon(image));
        }
    }
    /*
        Retourne le tableau de JLabels 
    */
    public JLabel[][] getPlateau(){
        return this.plateau;
    }

    /*
        Retourne une case du tableau
    */
    public JLabel getCase(int x, int y){
        return this.plateau[x][y];
    }
    public void setplateau(Board b) {
        this.b = b;
        this.grille = b.CopyGrille();

        for(int i=0; i<this.b.getHeight(); i++){
            for(int j=0; j<this.b.getWeight(); j++){
                JLabel cellule = this.plateau[i][j];
                // On paramètre l'affichage initiale d'une cellule
                cellule.setOpaque(true); 
                cellule.setBackground(new Color(229,185,120));
                Border border = BorderFactory.createLineBorder(Color.black);
                cellule.setBorder(border);
                cellule.setPreferredSize(new Dimension(40,40));
                // On dessine le contenu de la cellule en fonction des valeurs du modèle
                // Traitement effectué dans une fonction car nous aurons besoin de cette partie du code pour modifier l'affichage de la cible une fois qu'elle a été tirée.
                //System.out.print(grille[0][grille.length-1]);
                dessiner(cellule,i,j,true);
                this.plateau[i][j] = cellule;
                           
            }
        }
        repaint();
    }
    /***
 * La fonction qui nous permet de mettre à jour la vue 
   ***/
    @Override
    public void update(Observable o, Object arg) {
          //setplateau(b);
          //this.update();
    }

}
