package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Algorithm;
import model.Alphabeta;
import model.Board;
import model.Minimax;
import model.Paranoid;
import controller.Controller;
public class Vue extends JFrame  {

    private Board b;
    private JPanel container;
    private Plateau plateau;
    private Action interfaceUtilisateur;
    private Controller controller;

    private Thread thread;
    private Algorithm algo;
    public Vue(Board b,String algorithme,String prof1,String prof2){
        super("Jeu de Tron");
        this.b = b;
        //this.modele.addObserver(this);
        this.container = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        this.plateau = new Plateau(this.b);
        this.interfaceUtilisateur = new Action(this.b,this);
        JButton button=interfaceUtilisateur.getBouton();
        //String algorithme=v.getAlgorithme();
        System.out.println(prof1);
        System.out.println(prof2);

        int p1=Integer.parseInt(prof1);
        int p2=Integer.parseInt(prof2);
        button.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {

                    thread = new Thread(new Runnable(){
                    @Override
                    public void run(){

                        // Code à exécuter lorsque le bouton est cliqué
                        Vue.this.b.nextPlayer();
                        Vue.this.b.currentPlayer.setFirst();
                        if(algorithme.equals("Paranoid"))
                        {
                            algo=new Paranoid();
                            System.out.println("toto");
                        }
                        if(algorithme.equals("MinMax"))
                        {
                            System.out.println("hada"+algorithme);
                            algo=new Minimax();
                        }
                        if(algorithme.equals("AlphaBeta"))
                        {
                            algo=new Alphabeta();
                        }
                        String direction;
                        do{
                            Vue.this.b.setProfondeur(p1);
                            Vue.this.b.setProfondeurCoalition(p2);
                            Vue.this.b.isDead();
                            direction = algo.operation(Vue.this.b);
                            Vue.this.b.play(Vue.this.b.currentPlayer.getCouleur(),direction);
                            Vue.this.b.afficherGrid();
                            Vue.this.plateau.setplateau(Vue.this.b);

                            //Timer t=new Timer();
                            /*System.out.println("toto");

                            Vue.this.b.isDead();
                            direction = algo.operation(Vue.this.b);
                            System.out.println(direction);
                            Vue.this.b.play(Vue.this.b.currentPlayer.getCouleur(),direction);
                            Vue.this.plateau.setplateau(Vue.this.b);
                            Vue.this.b.afficherGrid();*/
                        }while(!Vue.this.b.terminal());
                    }
                });
                thread.start();
            }
         });
        this.add(container);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(5,5,5,5);
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 0;
        this.container.add(new JLabel("Jeu de Tron"),c);
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 1;
        this.container.add(plateau,c);
        c.gridx = 1;
        this.container.add(interfaceUtilisateur,c);

        this.setSize(900,800);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    

    /**
     * @return le composant interfaceUtilisateur
     */
    public Action getInterfaceUtilisateur() {
        return interfaceUtilisateur;
    }

    public void updatep(Board b)
    {
        plateau=new Plateau(b);
        plateau.repaint();
    }
    public static void main(String[] args) {
		
		//Board b1=new Board(4);
		//Vue v=new Vue(b1);
        Vue1 v=new Vue1();
        //Controller c=new Controller(v, b1);
	}

}