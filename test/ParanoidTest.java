package model.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.Board;
import model.Paranoid;
import model.Player;

public class ParanoidTest {
    @Test
    public void testOperation() {
        Board b = new Board();
        Player player1 = new Player(6, 4, "R");
        Player player2 = new Player(4, 2, "N");
        b.getPlayers().add(player1);
        b.getPlayers().add(player2);
        b.placerPlayerinit();
        Paranoid pd = new Paranoid();
        int p = pd.paranoid(b,3,player1);
        String result = pd.operation(b);
        assertTrue(result.equals("E") || result.equals("S") || result.equals("W"));


    
    }   
}
