package model.test;
import static org.junit.Assert.*;

import java.util.*;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.text.Position;

import org.junit.Test;
import model.*;


public class BoardTest {
   
    


   @Test
   public void testPlacerPlayerinit() {
       // Créez une instance de la classe qui contient la méthode à tester
       Board b = new Board(3);
    //    List<Player> players = new  ArrayList<Player>(); 
      
       // Ajoutez les joueurs à la liste players de l'instance de la classe
       Player player1 = new Player("R");
       Player player2 = new Player("B");
       Player player3 = new Player("G");
       b.getPlayers().add(player1);
       b.getPlayers().add(player2);
       b.getPlayers().add(player3);
       
       // Appelez la méthode placerPlayerinit()
       b.placerPlayerinit();
       
       // Vérifiez que les joueurs ont été placés sur la grille de manière aléatoire
       for (Player p : b.getPlayers()) {
           assertTrue(p.getX()>= 0 && p.getX()< b.getHeight() || p.getY() >= 0 && p.getY()< b.getWeight());
        //    assertTrue(p.getY() >= 0 && p.getY()< b.weight);
        //    assertEquals(b.grille()[p.getX()][p.getY()], p.getCouleur());
       }
   }





   @Test
    public void testIsCellMovableTo() {
      
        Board b = new Board(3);

        // Test North direction
        assertFalse(b.isCellMovableTo(1, 1, "N"));
        assertTrue(b.isCellMovableTo(2, 1, "N"));

        // Test South direction
        assertFalse(b.isCellMovableTo(b.getHeight() - 2, 1, "S"));
        assertTrue(b.isCellMovableTo(b.getHeight() - 3, 1, "S"));

        // Test East direction
        assertFalse(b.isCellMovableTo(1, b.getWeight() - 2, "E"));
        assertTrue(b.isCellMovableTo(1, b.getWeight() - 3, "E"));

        // Test West direction
        assertFalse(b.isCellMovableTo(1, 1, "W"));
        assertTrue(b.isCellMovableTo(1, 2, "W"));

        // Test invalid direction
        assertFalse(b.isCellMovableTo(5, 5, "invalid direction")); 

    }
    
    @Test
    public void testPosArrive() {
          
        Board b = new Board(3);
        
        ArrayList<Integer> result = b.posArrive(5, 5, "N");
        int a=result.get(0);
        int j =result.get(1);

        assertEquals(4,a);
        assertEquals(5,j);
        
        result = b.posArrive(5, 5, "E");
        int n=result.get(0);
        int m =result.get(1);
        // System.out.println(result);
        assertEquals(5, n);
        assertEquals(6, m);
        
        result = b.posArrive(5, 5, "S");
        int r=result.get(0);
        int l =result.get(1);
        assertEquals(6, r);
        assertEquals(5, l);
        
        result = b.posArrive(5, 5, "W");
        int p=result.get(0);
        int q =result.get(1);
        assertEquals(5, p);
        assertEquals(4,q);
    }
    @Test
    public void testCopyPlayer() {
        Board b = new Board(2);
        Player p = new Player(5,6,"R");
        Player copy = b.CopyPlayer(p);
    
        assertEquals(p.getCouleur(), copy.getCouleur());
        assertEquals(p.getX(), copy.getX());
        assertEquals(p.getY(), copy.getY());
        assertEquals(p.isFirst(), copy.isFirst());
      }
     @Test
      public void testDijkstra() {
          int[][] positions = {
                  {0, 2, 4},
                  {2, 0, 6},
                  {4, 6, 0}
          };
          int[][] expectedPositions = {
                  {0, 2, 4},
                  {2, 0, 6},
                  {4, 6, 0}
          };
          int row = 0;
          int col = 0;
          int iteration = 0;
          
          Board b = new Board(2);
      
          int[][] result = b.dijkstra(positions, iteration, row, col);
      
          assertArrayEquals(expectedPositions, result);
      }



      @Test
      public void testValue() {
        Board board = new Board(2); // crée un plateau de jeu pour 2 joueurs
        Player player1 = new Player("R"); // crée un joueur rouge
        Player player2 = new Player("B"); // crée un joueur bleu
        ArrayList<Player> p = board.getPlayers();
        p.add(player1);
        p.add(player2);
        // Placez quelques joueurs sur le plateau de jeu pour que chaque joueur contrôle certains domaines
        board.placerPlayerinit(); // joueur rouge contrôle un domaine
       
        
        // Vérifiez la valeur des domaines contrôlés par chaque joueur
        assertEquals(board.domainCount(player1), board.value(player1)); // joueur rouge contrôle un domaine, donc sa valeur est 1
        assertEquals(board.domainCount(player2), board.value(player2)); // joueur bleu contrôle un domaine, donc sa valeur est 1
        
        // Placez une pierre supplémentaire pour que le joueur rouge contrôle un deuxième domaine
        board.play("R","w"); // joueur rouge contrôle un deuxième domaine
        board.play("B","E");
        // Vérifiez la valeur des domaines contrôlés par chaque joueur
        assertEquals(board.domainCount(player1), board.value(player1)); // joueur rouge contrôle deux domaines, donc sa valeur est 2
        assertEquals(board.domainCount(player2), board.value(player2)); // joueur bleu contrôle toujours un seul domaine, donc sa valeur est 1
        //---------------------------------------------------------------
        //essyer avec la creation d une autre methode play avec argument player et direction dans la classe Board
        //---------------------------------------------------------------------------------------

    }
    @Test
    public void testIsInBoard() {
        Board board = new Board(5, 15); // Créer un objet Board de 5x5
        // Vérifier si des coordonnées sont dans le plateau
        assertTrue(board.isInBoard(0, 0)); // Le coin supérieur gauche est dans le plateau
        assertTrue(board.isInBoard(14, 14)); // Le coin inférieur droit est dans le plateau
        assertTrue(board.isInBoard(12, 13)); // Les coordonnées du milieu sont dans le plateau
        assertFalse(board.isInBoard(-1, 2)); // Les coordonnées négatives ne sont pas dans le plateau
        
    }


@Test
public void testPlay() {
    Board b = new Board(2);
    Player player1=new Player("B");
    Player player2=new Player("N");
    b.getPlayers().add(player1);
    b.getPlayers().add(player2);
    
    
    //placer tt les joueur dans la grille 
       b.placerPlayerinit();
       int posIX1=b.getPlayer1("B").getX();
       int posIY1=b.getPlayer1("B").getY();
       int posIX2=b.getPlayer2("N").getX();
       int posIY2=b.getPlayer2("N").getY();
  
    // déplacer le joueur blanc vers la droite
    // déplacer le joueur noir ves la gauche 
    b.play("B", "E");
    b.play("N","W");
    //garder les positions des jouers apres deplacement 
    int posPX1=b.getPlayer1("B").getX();
    int posPY1=b.getPlayer1("B").getY();
    int posPX2 = b.getPlayer2("N").getX();
    int posPY2=b.getPlayer2("N").getY();

    // vérifier que le joueur blanc a été déplacé dans la grille 
    assertFalse(b.getGrille()[posIX1][posIY1]== b.CopyGrille()[posPX1][posPY1]);
    assertFalse(b.getGrille()[posIX2][posIY2]== b.CopyGrille()[posPX2][posPY2]);
   
    
    // vérifier que le tour de jeu a été changé
    System.out.println(b.getCurrentPlayer().getCouleur());
    assertEquals("2",b.getCurrentPlayer().getCouleur());
}

@Test
public void testDomainCount() { 
    Board b = new Board(3);
 
    Player player1 = new Player(0, 0,"N");
    Player player2 = new Player(5, 5,"B");
    Player player3 = new Player(9, 9,"V");
    b.getPlayers().add(player1);
    b.getPlayers().add(player2);
    b.getPlayers().add(player3);
   

    int count1 = b.domainCount(player1);
    assertEquals(0, count1);

    int count2 = b.domainCount(player2);
    System.out.print("hey");
    assertEquals(0, count2);

    int count3 = b.domainCount(player3);
    assertEquals(2, count3);

    // Player player4 = new Player(2, 2,"R");
    // b.getPlayers().add(player4);

    // int count4 = b.domainCount(player4);
    // assertEquals(2, count4);
}








    }

   






