package model.test;

import static org.junit.Assert.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.text.Position;

import org.junit.Test;
import model.*;

public class AlphabetaTest {

    @Test

    public void testOperation() {
        Board board = new Board(2);
        Player player1 = new Player(2, 3, "R");
        Player player2 = new Player(6, 2, "B");
        board.getPlayers().add(player1);
        board.getPlayers().add(player2);
        board.placerPlayerinit();

        Alphabeta alphabeta = new Alphabeta();
        // appel de la methode de l'operation
        board.isDead();
        String bestDirection = alphabeta.operation(board);

        // Vérification que le meilleur mouvement est valide
        assertNotNull(bestDirection);
        // verifier que la meilleur possition et parmis les ossition possible
        String[] possibleDirections = { "N", "E", "S", "W" };
        assertTrue(Arrays.asList(possibleDirections).contains(bestDirection));
    }

}

