package model.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import model.*;
public class PlayerTest {
    @Test
    public void testConstructor() {
        Player p = new Player("R");
        assertEquals("R", p.getCouleur());
        assertFalse(p.getPerdu());
        assertFalse(p.isFirst());
        
        Player q = new Player(3, 4, "B");
        assertEquals(3, q.getX());
        assertEquals(4, q.getY());
        assertEquals("B", q.getCouleur());
        assertFalse(q.isFirst());
    }

    @Test
    public void testSettersAndGetters() {
        Player p = new Player("R");
        p.setX(2);
        assertEquals(2, p.getX());
        p.setY(5);
        assertEquals(5, p.getY());
        p.setCouleur("J");
        assertEquals("J", p.getCouleur());
        p.setPerdu(true);
        assertTrue(p.getPerdu());
        p.setFirst(true);
        assertTrue(p.isFirst());
    }



    
}

