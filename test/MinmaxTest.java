package model.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.Board;
import model.Minimax;
import model.Player;

public class MinimaxTest {
@Test
public void testOperation() {
    Board b = new Board();
    Player player1 = new Player(4, 4, "R");
    Player player2 = new Player(6, 2, "N");
    b.getPlayers().add(player1);
    b.getPlayers().add(player2);
    b.placerPlayerinit();
    b.currentPlayer = b.getCurrentPlayer(); // définit le joueur actuel
    Minimax mn = new Minimax();
    String result = mn.operation(b); // exécute la méthode "operation"

    // Vérifie que la méthode renvoie une direction valide
    assertTrue(result.equals("N") || result.equals("E") || result.equals("S") || result.equals("W"));

    // Vérifie que la méthode a été appelée un nombre de fois raisonnable
    long startTime = System.nanoTime();
    result = mn.operation(b);
    long endTime = System.nanoTime();
    long duration = (endTime - startTime) / 1000000; // en millisecondes
    assertTrue(duration < 1000); // vérifie que la méthode a été exécutée en moins d'une seconde
    int a = mn.minimax(b,3,player1);
    // Vérifier que la méthode renvoie la direction qui maximise le score
    assertTrue(mn.operation(b).equals("E"));
}
    
}

